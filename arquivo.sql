-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: bancogenerico
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Agencia`
--

DROP TABLE IF EXISTS `Agencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Agencia` (
  `codigo` int(11) NOT NULL,
  `numero` varchar(45) NOT NULL,
  `Banco_codigo` int(11) NOT NULL,
  PRIMARY KEY (`codigo`,`Banco_codigo`),
  KEY `fk_Agencia_Banco1_idx` (`Banco_codigo`),
  CONSTRAINT `fk_Agencia_Banco1` FOREIGN KEY (`Banco_codigo`) REFERENCES `Banco` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Agencia`
--

LOCK TABLES `Agencia` WRITE;
/*!40000 ALTER TABLE `Agencia` DISABLE KEYS */;
INSERT INTO `Agencia` VALUES (1,'40555',1),(2,'14321',1),(3,'54551',1),(4,'46566',2),(5,'12368',3),(6,'13254',4),(7,'55315',5),(8,'41455',5),(9,'13255',6),(10,'12467',6),(11,'12356',2),(12,'16789',4),(13,'55523',5);
/*!40000 ALTER TABLE `Agencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Banco`
--

DROP TABLE IF EXISTS `Banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Banco` (
  `codigo` int(11) NOT NULL,
  `cnpj` varchar(45) NOT NULL,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Banco`
--

LOCK TABLES `Banco` WRITE;
/*!40000 ALTER TABLE `Banco` DISABLE KEYS */;
INSERT INTO `Banco` VALUES (1,'70.685.996/0001-00','Azul'),(2,'57.018.775/0001-01','Laranja'),(3,'12.860.935/0001-27','Roxo'),(4,'36.437.274/0001-90','Vermelho'),(5,'04.485.403/0001-29','Amarelo'),(6,'19.353.860/0001-37','Verde');
/*!40000 ALTER TABLE `Banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cliente`
--

DROP TABLE IF EXISTS `Cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cliente` (
  `cpf` double NOT NULL,
  `nome` varchar(45) NOT NULL,
  `logradouro` varchar(45) NOT NULL,
  `endereco` varchar(225) NOT NULL,
  `numero` varchar(45) NOT NULL,
  `cep` varchar(45) NOT NULL,
  `Agencia_codigo` int(11) NOT NULL,
  PRIMARY KEY (`cpf`,`Agencia_codigo`),
  KEY `fk_Cliente_Agencia_idx` (`Agencia_codigo`),
  CONSTRAINT `fk_Cliente_Agencia` FOREIGN KEY (`Agencia_codigo`) REFERENCES `Agencia` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cliente`
--

LOCK TABLES `Cliente` WRITE;
/*!40000 ALTER TABLE `Cliente` DISABLE KEYS */;
INSERT INTO `Cliente` VALUES (5377931785,'Carlos','alameda','pavao','681','04512-035',6),(36554887857,'Joao','rua','guara','112','04512-030',3),(36850807170,'Maria','alameda','urubu','5125','04512-033',12),(61247071448,'Ze','alameda','canario','671','04512-034',4),(67371868749,'Ze','rua','lobo','2313','04512-031',3),(72480566854,'Mario','rua','circo','24','04512-036',8),(87876738869,'Paula','rua','pedreira','1241','04512-032',5);
/*!40000 ALTER TABLE `Cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Gerente`
--

DROP TABLE IF EXISTS `Gerente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Gerente` (
  `funcional` int(11) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `cpf` varchar(45) NOT NULL,
  `Agencia_codigo` int(11) NOT NULL,
  PRIMARY KEY (`funcional`,`Agencia_codigo`),
  KEY `fk_Gerente_Agencia1_idx` (`Agencia_codigo`),
  CONSTRAINT `fk_Gerente_Agencia1` FOREIGN KEY (`Agencia_codigo`) REFERENCES `Agencia` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Gerente`
--

LOCK TABLES `Gerente` WRITE;
/*!40000 ALTER TABLE `Gerente` DISABLE KEYS */;
INSERT INTO `Gerente` VALUES (123,'Joao','949.709.400-02',1),(124,'Maria','984.402.930-97',2),(125,'Paula','773.714.390-40',3),(126,'Paulo','976.147.680-43',4),(127,'Ze','610.121.500-89',5),(128,'Carlos','134.968.560-76',6);
/*!40000 ALTER TABLE `Gerente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Telefone`
--

DROP TABLE IF EXISTS `Telefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Telefone` (
  `codigo` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `numero` varchar(45) NOT NULL,
  `Cliente_cpf` double NOT NULL,
  PRIMARY KEY (`codigo`,`Cliente_cpf`),
  KEY `fk_Telefone_Cliente1_idx` (`Cliente_cpf`),
  CONSTRAINT `fk_Telefone_Cliente1` FOREIGN KEY (`Cliente_cpf`) REFERENCES `Cliente` (`cpf`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Telefone`
--

LOCK TABLES `Telefone` WRITE;
/*!40000 ALTER TABLE `Telefone` DISABLE KEYS */;
INSERT INTO `Telefone` VALUES (1,'residencial','98723-3545',36554887857),(2,'comercial','98723-3546',36554887857),(3,'celular','98723-3547',36554887857);
/*!40000 ALTER TABLE `Telefone` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-18 11:39:16
